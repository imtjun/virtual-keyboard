# virtual-keyboard
web 虚拟键盘， jquery


使用示例：

```
var KB = new KeyBoard().forAllInput();
```
属性：
    无
     
可用方法：

    forAllInput: 如：KB.forAllInput(), 执行此方法后，当页面上有的input被点击时会显示键盘，并把被点击input作为输入目标.
    inputTo: 指定键盘输入目标，如：KB.inputTo("#inputTest");


![输入图片说明](https://gitee.com/uploads/images/2018/0319/113257_52207f1b_1496388.jpeg "keyboard.jpg")

